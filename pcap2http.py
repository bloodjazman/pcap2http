#!/usr/bin/env python
# coding=utf-8
import sys
import re
import argparse
import logging
import subprocess
import io
import os
import StringIO

try:
    from http_parser.parser import HttpParser
except ImportError:
    from http_parser.pyparser import HttpParser

import http_parser.util

clean_name = re.compile('[^0-9a-z\.]+')
clean_symbols = re.compile('_+')


def shell(cmd, capture_stderr=False, capture_stdout=False, expected_exit_code=0):
    logging.debug('Shell: ' + cmd)
    stderr = StringIO.StringIO
    stdout = StringIO.StringIO

    if not capture_stdout:
        stdout = sys.stdout
    if not capture_stderr:
        stderr = sys.stderr

    exit_code = subprocess.call(cmd, stdout=stdout, stderr=stderr, shell=True)

    try:
        if exit_code not in expected_exit_code:
            raise RuntimeError('%s exit code = %s', (cmd, exit_code))
    except TypeError:
        if exit_code != expected_exit_code:
            raise RuntimeError('%s exit code = %s', (cmd, exit_code))

    if capture_stdout or capture_stderr:
        return {'exit_code': exit_code, 'stdout': stdout, 'stderr': stderr}
    else:
        return {'exit_code': exit_code, 'stdout': '', 'stderr': ''}


def clearify(name):
    return clean_symbols.sub('_', clean_name.sub('_', name))


def extract_http_from_pcap(pcap_file, output_dir):
    flow_fields = (
        '%source.ip:%source.port %-> %dest.ip:%dest.port',
        'TIME %request.timestamp',
        'REQUEST TIME %request.time sec',
        'CONNECTION TIME %connection.time sec',
        'RESPONSE TIME %response.time sec',
    )
    flow_format = '%%==[FLOW]==%newlineW' + '%newlineW'.join(flow_fields)
    request_format = '%%==[REQUEST]==%newline%request'
    response_format = '%%==[RESPONSE]==%newline%response'
    end_format = '%newline%%==[END]==%newline'
    log_format = flow_format + request_format + response_format + end_format

    out_buffer = io.BytesIO()
    in_buffer = io.BytesIO()
    state = ''
    response = subprocess.Popen(
        ['/usr/bin/justniffer', '-f', pcap_file, '-l', log_format], stdout=in_buffer, stderr=subprocess.PIPE
    )
    response.communicate()

    out_file = None
    body = []
    parser = None
    while True:
        line = in_buffer.read()

        if not line:
            break

        if line.startwith('%==['):
            state = line.replace('%==[', '').replace(']==', '')
            out_buffer.write(line)
            body = []
            continue
        if state == 'FLOW':
            parser = HttpParser()
            file_name = os.path.join(output_dir, clearify(line))
            out_file = open(file_name, 'wb')
            out_file.writelines(out_buffer.readlines())
            out_file.write(line)
            state = 'WAIT_NEXT_SECTION_'
            continue
        if state == 'WAIT_NEXT_SECTION':
            out_file.write(line)
            continue
        if state == 'REQUEST' or state == 'RESPONSE':
            out_file.writelines(out_buffer.readlines())
            parsed = parser.execute(line, len(line))
            assert parsed == len(line)
            if parser.is_headers_complete():
                for header, value in parser.get_headers().iteritems():
                    out_file.write(':'.join([header, value]))

            if parser.is_partial_body():
                body.append(parser.recv_body())
            if parser.is_message_complete():
                out_file.write(http_parser.util.b("").join(body))

        if state == 'END':
            out_file.writelines(out_buffer.readlines())
            out_file.close()
            out_file = None
            parser = None
            body = []


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script for aggregation PINBA metrics to GRAPHITE')
    parser.add_argument(
        '--pcap-file', metavar='FILENAME',
        required=True, type=str, help='path to pcap file',
    )
    parser.add_argument(
        '--output-dir', metavar='FILENAME',
        required=True, type=str, help='path to pcap file',
    )
    parser.add_argument(
        '--verbose', default=False, required=False, type=bool, help='verbose mode',
    )

    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    out = StringIO.StringIO
    response = shell('justniffer --version | grep justniffer | wc -l', capture_stdout=True)
    if response['stdout'].readall() != '1':
        raise RuntimeError('please run apt-get install justniffer')

    extract_http_from_pcap(args.pcap_file, args.output_dir)
